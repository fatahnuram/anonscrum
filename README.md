# AnonScrum

Anonymous scrum with modified rules. Just do your task, don't judge others!

## Notes

You don't need to know all about scrum, or being a good Scrum Master. But if you already have it, then it's a good point to start! :stuck_out_tongue_winking_eye:

This is just sharing a crazy idea though! Don't judge on me, please! I'm just trying to implement all of these customized scrum of mine :grin:

If you want to contribute, then you're welcomed to do so.

## The Big Rules

Read the rules to see all used (and modified) rules of conventional scrum! This is purely a crazy idea going through my brain :grin:

## LICENSE

![WTFPL](http://www.wtfpl.net/wp-content/uploads/2012/12/wtfpl-badge-4.png "WTFPL")

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

Copyright (C) 2018 Fatah Nur Alam Majid <fatahnuram@gmail.com>

Everyone is permitted to copy and distribute verbatim or modified
copies of this license document, and changing it is allowed as long
as the name is changed.

           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
  TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

 0. You just DO WHAT THE FUCK YOU WANT TO.