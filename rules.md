# The Big Rules

This is the customized rules from conventional scrum methodology. If anything feels wrong or maybe you have an idea about this, you can contact me :smile:

Once more, I'm only sharing an idea of mine :smile:

1. **THERE ARE NO RESTROSPECTIVE MEETUP.** We're just doing it remotely and anonymously. Just write all of your opinion into the board, don't mention that specific user, but mention their roles (the whole users with same role will notified by this). This can be implemented by adding a new Section called _The Restrospective_ to includes all of the mentions.

2. **THERE ARE NO DAILY SCRUM.** If you want to discuss about something, you can talk to the _Scrum Master_ directly, by face-to-face or by remote or by chat.

3. **THERE ARE NO SPRINTS.** Yes you heard that, there are no sprints. This methodology is likely a Kanban rather than being a Scrum. This is because there are no daily scrum included if you're trying to implement this methodology.

4. **You don't need to know who is doing that task, just do what you can do.** The only thing you must care about is to complete all you can do in the _Sprint Planning_ and contribute the most. If you want to do some task, just pin yourself into that card (anonymously) and drag the card into _In Progress_.

5. **You must listen to _Scrum Master_.** This because SM will become the most critical roles here. SM must know what technologies to use, how does the app work, what's the plus and minus of any technologies to use, etc. So it's basically if you want to be the SM, you must start from the zero or from being one of the _Developers_.

6. **The only one knowing all members of the team is the _Scrum Master_, neither _Product Owner_ nor  _Developers_ knows anybody except the _Scrum Master_ himself.** This _Scrum Master_ role is a critical role in this context. That's because if you're the SM, you must know all about technical flow of the app and if it's possible to implement some newly added features (from _Product Owner_ to the _Developers_). It's a critical thing that you know all the aspect off all technologies used for this app, so you can determine what features are possible and what features aren't.

7. **If you're the _Scrum Master_, go meet the _Prodcut Owner_ and the _Developers_ one-by-one.** This is mandatory for you if you're the SM. You must do this to know all of the team members, and to discuss about the project.

8. **If you're the _Scrum Master_, you must be sure of what features to be implemented.** You must know what feature(s) that's possible to be implemented or the feature(s) that don't. If you think you're skilled enough, you can decide it yourself and make your _Devs_ implement that feature(s). Otherwise, you can add a section called _The Feature Votes_ to votes what feature(s) is really can be implemented by your _Devs_ or what feature(s) that's not possible to be implemented right now. So the _Devs_ can do votes anonymously, and will know what they will be doing after the votes completed.

9. **Just do your task, and contribute the most.** No judging others, no blaming others. If you found any bugs or any ideas, just write them into the card so the appropriate anonymous user will be notified from your actions. Once again, no judging, no blaming.